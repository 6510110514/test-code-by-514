def caesarCipher(s, k):
    za = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    az = "abcdefghijklmnopqrstuvwxyz"
    sum = ''
    for i in s :
        if i in az  :
            w = az.find(i)
            x = az[(az.find(i)+k)%26]
            sum+=x
        elif i in za  :
            w = za.find(i)
            x = za[(za.find(i)+k)%26]
            sum+=x
        else :
            sum+=i
    return sum
