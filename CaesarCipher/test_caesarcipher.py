from caesarcipher import caesarCipher

import unittest


class alternatingTest(unittest.TestCase):
    def test_give_middle_Outz_is_prime(self):
        s = 'middle-Outz'
        k = 2
        is_prime = caesarCipher(s,k)
        self.assertTrue(is_prime)

    def test_give_Always_Look_on_the_Bright_Side_of_Life_is_prime(self):
        s = 'Always-Look-on-the-Bright-Side-of-Life'
        k = 5
        is_prime = caesarCipher(s,k)
        self.assertTrue(is_prime)
