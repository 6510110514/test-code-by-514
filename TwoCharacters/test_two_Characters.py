from two_Characters import alternate

import unittest


class alternatingTest(unittest.TestCase):
    def test_give_beabeefeab_is_prime(self):
        s = 'beabeefeab'
        is_prime = alternate(s)
        self.assertTrue(is_prime)

    def test_give_asdcbsdcagfsdbgdfanfghbsfdab_is_prime(self):
        s = 'asdcbsdcagfsdbgdfanfghbsfdab'
        is_prime = alternate(s)
        self.assertTrue(is_prime)
    
    def test_give_asvkugfiugsalddlasguifgukvsa_is_prime(self):
        s = 'asvkugfiugsalddlasguifgukvsa'
        is_prime = alternate(s)
        self.assertTrue(is_prime)

