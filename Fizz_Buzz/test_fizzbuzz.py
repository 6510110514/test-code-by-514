from fizzbuzz import Fizbuzz_fac

import unittest


class FizzBuzzTest(unittest.TestCase):
    def test_fizz_is_prime(self):
        num = 3
        is_prime = Fizbuzz_fac(num)
        self.assertTrue(is_prime)

    def test_buzz_is_prime(self):
        num = 5
        is_prime = Fizbuzz_fac(num)
        self.assertTrue(is_prime)
    
    def test_fizz_buzz_is_prime(self):
        num = 15
        is_prime = Fizbuzz_fac(num)
        self.assertTrue(is_prime)

    def test_non_fizz_buzz_is_prime(self):
        num = 2
        is_prime = Fizbuzz_fac(num)
        self.assertTrue(is_prime)