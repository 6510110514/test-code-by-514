from alternating import alternatingCharacters

import unittest


class alternatingTest(unittest.TestCase):
    def test_give_AAAA_is_prime(self):
        s = 'AAAA'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)

    def test_give_BBBBB_is_prime(self):
        s = 'BBBBB'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)

    def test_give_ABABABAB_is_prime(self):
        s = 'ABABABAB'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)

    def test_give_BABABA_is_prime(self):
        s = 'BABABA'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)

    def test_give_AAABBB_is_prime(self):
        s = 'AAABBB'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)

    def test_give_AAABBBAABB_is_prime(self):
        s = 'AAABBBAABB'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)

    def test_give_AABBAABB_is_prime(self):
        s = 'AABBAABB'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)

    def test_give_ABABABAA_is_prime(self):
        s = 'ABABABAA'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)

    def test_give_ABBABBAA_is_prime(self):
        s = 'ABBABBAA'
        is_prime = alternatingCharacters(s)
        self.assertTrue(is_prime)