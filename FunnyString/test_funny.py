from funny import funnyString

import unittest


class FunnyTest(unittest.TestCase):
    def test_give_Funny_is_prime(self):
        s = 'acxz'
        is_prime = funnyString(s)
        self.assertTrue(is_prime)

    def test_give_NOT_funny_is_prime(self):
        s = 'bcxz'
        is_prime = funnyString(s)
        self.assertTrue(is_prime)
