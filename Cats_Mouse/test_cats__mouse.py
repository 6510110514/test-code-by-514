from cats__mouse import catAndMouse

import unittest


class Cats_Mouse_Test(unittest.TestCase):
    def test_give_Mouse_is_prime(self):
        x = 1
        y = 2 
        z = 3
        is_prime = catAndMouse(x,y,z)
        self.assertTrue(is_prime)

    def test_give_Cat_B_is_prime(self):
        x = 1 
        y = 3
        z = 2
        is_prime = catAndMouse(x,y,z)
        self.assertTrue(is_prime)

    def test_give_Cat_A_is_prime(self):
        x = 2
        y = 3
        z = 1
        is_prime = catAndMouse(x,y,z)
        self.assertTrue(is_prime)

