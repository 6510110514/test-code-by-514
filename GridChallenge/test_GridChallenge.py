from GridChallenge import gridChallenge

import unittest


class alternatingTest(unittest.TestCase):
    def test_give_test_0_is_prime(self):
        grid = ['eabcd','fghij','olkmn','trpqs','xywuv']
        is_prime = gridChallenge(grid)
        self.assertTrue(is_prime)

    def test_give_test_1_is_prime(self):
        grid = ['abc','lmp','qrt']
        is_prime = gridChallenge(grid)
        self.assertTrue(is_prime)

    def test_give_test_2_is_prime(self):
        grid = ['mpxz','abcd','wlmf']
        is_prime = gridChallenge(grid)
        self.assertTrue(is_prime)
    
    def test_give_test_3_is_prime(self):
        grid = ['abc','hjk','mpq','rtv']
        is_prime = gridChallenge(grid)
        self.assertTrue(is_prime)

    

